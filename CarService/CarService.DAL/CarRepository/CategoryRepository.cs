﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarService.DAL
{
    public class CategoryRepository : IDBOperations<Category>
    {
        public IEnumerable<Category> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Categories.ToList();
            }
        }

        public Category GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.Categories.FirstOrDefault(c => c.Id == id);
            }
        }

        public Category Update(Category item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Category category = context.Categories.FirstOrDefault(c => c.Id == item.Id);

                category.Name = item.Name;

                context.SaveChanges();

                return category;
            }
        }

        public Category Create(Category item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                Category category = new Category()
                {
                    Name = item.Name
                };

                category = context.Categories.Add(category);

                context.SaveChanges();

                return category;
            }
        }
    }
}