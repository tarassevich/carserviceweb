﻿using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarService.DAL
{
    public class EventTypesRepository : IDBOperations<EventType>
    {
        public EventType Create(EventType item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                EventType eventType = new EventType()
                {
                    Name = item.Name,
                };

                eventType = context.EventTypes.Add(eventType);

                context.SaveChanges();

                return eventType;
            }
        }

        public IEnumerable<EventType> GetAll()
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.EventTypes.ToList();
            }
        }

        public EventType GetById(int id)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                return context.EventTypes.FirstOrDefault(t => t.Id == id);
            }
        }

        public EventType Update(EventType item)
        {
            using (CarServiceDbContext context = new CarServiceDbContext())
            {
                EventType eventType = context.EventTypes.FirstOrDefault(t => t.Id == item.Id);

                eventType.Name = item.Name;
                
                context.SaveChanges();

                return eventType;
            }
        }
    }
}