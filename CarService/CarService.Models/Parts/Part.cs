﻿using System.Collections.Generic;

namespace CarService.Models
{
    public class Part
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string SerialNumber { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public virtual List<Event> Events { get; set; }
    }
}
