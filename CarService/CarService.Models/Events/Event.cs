﻿using System;
using System.Collections.Generic;

namespace CarService.Models
{
    public class Event
    {
        public int Id { get; set; }
        public DateTime? FailureDate { get; set; }
        public DateTime? RepairDate { get; set; }
        public string Description { get; set; }

        public int EventTypeId { get; set; }
        public virtual EventType EventType { get; set; }

        public int ServiceId { get; set; }
        public virtual Service Service { get; set; }

        public int CarId { get; set; }
        public virtual Car Car { get; set; }

        public virtual List<Part> Parts { get; set; }

        public Event()
        {
            Parts = new List<Part>() { };
        }
    }
}
