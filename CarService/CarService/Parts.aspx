﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Parts.aspx.cs" Inherits="CarService.Parts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Parts</h1>
    <asp:GridView ID="Parts_GridView" runat="server" OnRowEditing="Parts_GridView_RowEditing"  AutoGenerateEditButton="true"></asp:GridView>
    <hr />

    <asp:Label ID="PartIdLabel" runat="server" Text="" Visible="false"></asp:Label>
    <div class="width-300">
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="NameLabel" runat="server" Text="Name"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="NameTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="SerialNumberLabel" runat="server" Text="Serial Number"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="SerialNumberTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="PriceLabel" runat="server" Text="Price"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="PriceTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="DescriptionLabel" runat="server" Text="Description"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="DescriptionTextBox" runat="server"></asp:TextBox></div>
        </div>
        <center>
            <asp:Button ID="AddPartButton" OnClick="AddPartButton_Click" runat="server" Text="Save" />
            <asp:Button ID="UpdatePartButton" OnClick="UpdatePartButton_Click" runat="server" Text="Update" Visible ="false" />
            <asp:Button ID="ClearPartFormButton" OnClick="ClearPartFormButton_Click" runat="server" Text="Clear" Visible ="false" />
        </center>
        <div>
            <asp:RequiredFieldValidator id="PriceRequiredFieldValidator" runat="server"
                ControlToValidate="PriceTextBox"
                ErrorMessage="Price is a required field." ForeColor="Red"/>
        </div>
        <div>
            <asp:RangeValidator ID="PriceValidator" ControlToValidate="PriceTextBox" MinimumValue="0"
                MaximumValue="2147483647" Type="Double"
                Text="The value must be greater or equal than 0" runat="server" ForeColor="Red"/>
        </div>
    </div>
</asp:Content>
