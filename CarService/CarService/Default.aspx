﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CarService._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>My Cars</h1>
    <asp:Label ID="AddFirstCarLabel" Text="Please add your first car" runat="server" Visible="false"/>
    <asp:GridView OnRowEditing="Cars_GridView_RowEditing"  AutoGenerateEditButton="true" ID="Cars_GridView" runat="server"></asp:GridView>
    <hr />
    <h4>Add Car</h4>
    <asp:Label ID="CarIdLabel" runat="server" Text="" Visible="false"></asp:Label>
    <div class="width-300">
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="CarTypesLabel" runat="server" Text="Type"></asp:Label></div>
            <div class="col-xs-6"><asp:DropDownList ID="CarTypesDropDownList" runat="server"></asp:DropDownList></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="FuelLabel" runat="server" Text="Fuel"></asp:Label></div>
            <div class="col-xs-6"><asp:DropDownList ID="FuelDropDownList" runat="server"></asp:DropDownList></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="CarMarkLabel" runat="server" Text="Mark"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="CarMarkTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="CarModelLabel" runat="server" Text="Model"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="CarModelTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="VINLabel" runat="server" Text="VIN number"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="VINTextBox" runat="server"></asp:TextBox></div>
        </div>
        <div class="row">
            <div class="col-xs-6"><asp:Label ID="NoteLabel" runat="server" Text="Note"></asp:Label></div>
            <div class="col-xs-6"><asp:TextBox ID="NoteTextBox" runat="server"></asp:TextBox></div>
        </div>
        <center>
            <asp:Button ID="AddCarButton" OnClick="AddCarButton_Click" runat="server" Text="Save" />
            <asp:Button ID="UpdateCarButton" OnClick="UpdateCarButton_Click" runat="server" Text="Update" Visible ="false" />
            <asp:Button ID="ClearCarButton" OnClick="ClearCarButton_Click" runat="server" Text="Clear" Visible ="false" />
        </center>    
    </div>
</asp:Content>
