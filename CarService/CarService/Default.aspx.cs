﻿using CarService.DAL;
using CarService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarService
{
    public partial class _Default : Page
    {
        CarRepository _carRepo;

        protected void Page_Load(object sender, EventArgs e)
        {
            _carRepo = new CarRepository();

            if (!Page.IsPostBack)
            {
                pushCarsToGridView(_carRepo.GetAll());

                CarTypesDropDownList.DataSource = new CategoryRepository().GetAll();
                CarTypesDropDownList.DataValueField = "Id";
                CarTypesDropDownList.DataTextField = "Name";
                CarTypesDropDownList.DataBind();

                FuelDropDownList.DataSource = new FuelRepository().GetAll();
                FuelDropDownList.DataValueField = "Id";
                FuelDropDownList.DataTextField = "Name";
                FuelDropDownList.DataBind();
            }
        }

        protected void Cars_GridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;

            AddCarButton.Visible = false;
            UpdateCarButton.Visible = true;
            ClearCarButton.Visible = true;

            string columnIdValue = Cars_GridView.Rows[e.NewEditIndex].Cells[1].Text;

            int carForEdit = Convert.ToInt32(columnIdValue);

            //load car for edit
            Car car = _carRepo.GetById(carForEdit);

            CarIdLabel.Text = car.Id.ToString();

            FuelDropDownList.SelectedValue = car.Fuel.Id.ToString();
            CarTypesDropDownList.SelectedValue = car.Category.Id.ToString();
            CarMarkTextBox.Text = car.Mark;
            CarModelTextBox.Text = car.Model;
            VINTextBox.Text = car.VIN;
            NoteTextBox.Text = car.Note;
        }

        protected void AddCarButton_Click(object sender, EventArgs e)
        {
            Car car = new Car()
            {
                LastEditDateTime = DateTime.UtcNow,
                FuelId = Convert.ToInt32(FuelDropDownList.SelectedValue),
                CategoryId = Convert.ToInt32(CarTypesDropDownList.SelectedValue),
                Mark = CarMarkTextBox.Text,
                Model = CarModelTextBox.Text,
                VIN = VINTextBox.Text,
                Note = NoteTextBox.Text
            };

            car = _carRepo.Create(car);

            pushCarsToGridView(_carRepo.GetAll());

            clearEditForm();
        }

        protected void UpdateCarButton_Click(object sender, EventArgs e)
        {
            Car car = new Car()
            {
                Id = Convert.ToInt32(CarIdLabel.Text),
                LastEditDateTime = DateTime.UtcNow,
                FuelId = Convert.ToInt32(FuelDropDownList.SelectedValue),
                CategoryId = Convert.ToInt32(CarTypesDropDownList.SelectedValue),
                Mark = CarMarkTextBox.Text,
                Model = CarModelTextBox.Text,
                VIN = VINTextBox.Text,
                Note = NoteTextBox.Text
            };

            car = _carRepo.Update(car);

            pushCarsToGridView(_carRepo.GetAll());

            AddCarButton.Visible = true;
            UpdateCarButton.Visible = false;
            ClearCarButton.Visible = false;

            clearEditForm();
        }

        private void pushCarsToGridView(IEnumerable<Car> cars)
        {
            if(cars.ToList().Count == 0)
            {
                AddFirstCarLabel.Visible = true;
            }
            else
            {
                AddFirstCarLabel.Visible = false;
            }

            Cars_GridView.DataSource = cars.Select(c =>
                new {
                    Id = c.Id,
                    Category = c.Category.Name,
                    Mark = c.Mark,
                    Model = c.Model,
                    VIN = c.VIN,
                    Fuel = c.Fuel.Name,
                    Note = c.Note
                });

            Cars_GridView.DataBind();
        }

        protected void ClearCarButton_Click(object sender, EventArgs e)
        {
            clearEditForm();

            AddCarButton.Visible = true;
            UpdateCarButton.Visible = false;
            ClearCarButton.Visible = false;
        }

        private void clearEditForm()
        {
            CarIdLabel.Text = "";
            CarMarkTextBox.Text = "";
            CarModelTextBox.Text = "";
            VINTextBox.Text = "";
            NoteTextBox.Text = "";
        }
    }
}