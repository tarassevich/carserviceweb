﻿using CarService.DAL;
using CarService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarService
{
    public partial class StaticData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EventTypes_GridView.DataSource = new EventTypesRepository().GetAll();
            EventTypes_GridView.DataBind();

            Categories_GridView.DataSource = new CategoryRepository().GetAll();
            Categories_GridView.DataBind();

            Fuels_GridView.DataSource = new FuelRepository().GetAll();
            Fuels_GridView.DataBind();
        }
    }
}