﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarService.DAL;
using CarService.Models;

namespace CarService
{
    public partial class MyEvents : System.Web.UI.Page
    {
        EventRepository _eventRepo;


        protected void Page_Load(object sender, EventArgs e)
        {
            _eventRepo = new EventRepository();

            if (!Page.IsPostBack)
            {
                pushEventsToGrid(_eventRepo.GetAll());

                EventTypesDropDownList.DataSource = new EventTypesRepository().GetAll();
                EventTypesDropDownList.DataValueField = "Id";
                EventTypesDropDownList.DataTextField = "Name";
                EventTypesDropDownList.DataBind();
                    
                CarsDropDownList.DataSource = new CarRepository().GetAll()
                    .Select(c => new { Id = c.Id, Name = c.Mark + " " + c.Model + " (" + c.Fuel.Name + ")" });
                CarsDropDownList.DataValueField = "Id";
                CarsDropDownList.DataTextField = "Name";
                CarsDropDownList.DataBind();

                PartsListBox.DataSource = new PartRepository().GetAll()
                    .Select(p => new { Id = p.Id, Name = p.Name + " (" + p.Price + "$)" });
                PartsListBox.DataValueField = "Id";
                PartsListBox.DataTextField = "Name";
                PartsListBox.DataBind();

                ServiceDropDownList.DataSource = new ServiceRepository().GetAll();
                ServiceDropDownList.DataValueField = "Id";
                ServiceDropDownList.DataTextField = "Name";
                ServiceDropDownList.DataBind();
            }
        }

        protected void AddEventButton_Click(object sender, EventArgs e)
        {
            Event eventForAdd = new Event()
            {
                Description = DescriptionTextBox.Text,

                EventTypeId = Convert.ToInt32(EventTypesDropDownList.SelectedItem.Value),
                CarId = Convert.ToInt32(CarsDropDownList.SelectedItem.Value),
                ServiceId = Convert.ToInt32(ServiceDropDownList.SelectedItem.Value),
            };

            PartRepository partRepo = new PartRepository();

            foreach (int i in PartsListBox.GetSelectedIndices())
            {
                var val = Convert.ToInt32(PartsListBox.Items[i].Value);

                eventForAdd.Parts.Add(partRepo.GetById(val));
            }

            _eventRepo.Create(eventForAdd);

            pushEventsToGrid(_eventRepo.GetAll());
        }

        private void pushEventsToGrid(IEnumerable<Event> events)
        {
            MyEvents_GridView.DataSource = events.Select(e => new
            {
                Id = e.Id,
                Description = e.Description,
                EventType = e.EventType.Name,
                Service = e.Service.Name,
                Car = e.Car.Mark + " " + e.Car.Model + "(" + e.Car.VIN + ")",
                Parts = getPartsStringForGrid(e.Parts)
            });
            MyEvents_GridView.DataBind();
        }

        private string getPartsStringForGrid(List<Part> parts)
        {
            string result = string.Empty;

            foreach (Part part in parts)
            {
                result += part.Name + " (" + part.Price + ")";

                if (part != parts.Last())
                {
                    result += " ,";
                }
            }

            return result;
        }
    }
}